﻿namespace Openvidu.Net.Aspnet.Core;

public class OpenViduWebhookOption
{
    public List<(string key, string value)> AcceptHeaders { get; set; }
}